class parallelPlot extends Frame {
  
    Table data;
    float border = 20;
    int idx0=0, idx1=1;
    float minX1, maxX1,minX2, maxX2,minX3, maxX3,minX4, maxX4;
    float minY, maxY;
  parallelPlot( Table data ){
     this.data = data;
     minX1 = min(data.getFloatColumn(idx0));
     minX2 = min(data.getFloatColumn(idx1));
     minX3 = min(data.getFloatColumn(2));
     minX4 = min(data.getFloatColumn(3));

     maxX1 = max(data.getFloatColumn(idx0));
     maxX2 = max(data.getFloatColumn(idx1));
     maxX3 = max(data.getFloatColumn(2));
     maxX4 = max(data.getFloatColumn(3));


  }
 
  void  draw() {
  //int j = width/3+20;
  for (int i = 0; i < table.getRowCount(); i++) {
        TableRow r = table.getRow(i);
        axis();    //function call to draw axis
        labels(); // function call to draw labels
       float x1 = map(r.getFloat(0), minX1, maxX1, 2*width/3, height/2+20);
       float x2 = map(r.getFloat(1), minX2, maxX2, 2*width/3, height/2+20 );
       float x3 = map(r.getFloat(2), minX3, maxX3, 2*width/3, height/2+20 );
       float x4 = map(r.getFloat(3), minX4, maxX4, 2*width/3, height/2+20 );
       fill(156,198,223);
       noStroke();
       ellipse(width/2+100,x1,4,4);            // plot of column1 values
       ellipse(width-350,x2,4,4);             // plot of column2 values
       ellipse(width-200,x3,4,4);      // plot of column3 values
       ellipse(width-60,x4,4,4);       // plot of column4 values
       noFill();
       stroke(34,65,98);     
       line(width/2+100,x1,width-350,x2);
       stroke(200,65,89);     
       line(width-350,x2,width-200,x3);
       stroke(134,56,214);     
       line(width-200,x3,width-60,x4);
       stroke(0);
          noFill();
  }
}
void axis(){
line(width/2+100,height/2+20,width/2+100,height-20);
        line(width-350,height/2+20,width-350,height-20);
        line(width-200,height/2+20,width-200,height-20);
        line(width-60,height/2+20,width-60,height-20);
}

void labels(){
fill(233,34,99);
          text(table.getColumnTitle(idx0),width/2+80,height/2+15);
          text(table.getColumnTitle(idx1),width-370,height/2+15);
          text(table.getColumnTitle(2),width-220,height/2+15);
          text(table.getColumnTitle(3),width-80,height/2+15);
          fill(0);
          text("PARALLEL PLOT",width/2+5,2*height/3);
          noFill();         
}
}