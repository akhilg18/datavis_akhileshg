class barChart extends Frame {
  
    Table data;
    float border = 20;
    int idx0=0, idx1=1;
    float minX, maxX;
    float minY, maxY;
    barChart( Table data){
     this.data = data;
     minX = min(data.getFloatColumn(idx0));
     maxX = max(data.getFloatColumn(idx0));
     
     minY = min(data.getFloatColumn(idx1));
     maxY = max(data.getFloatColumn(idx1));

  }
  
void  draw() {
  for (int i = 0; i < 23; i++) {
        TableRow r = table.getRow(i);
       float x = map(r.getFloat(idx0), minX, maxX,  u0 + border, u0 + w - border);   
       float y = map(r.getFloat(idx1), minY, maxY, v0 + v0+border,2*height/3 - border );
       labels();                      //function to draw labels
        stroke( 0 );
        fill(255,0,0);
        rect(25 + i * 15, height/2-20 - y, 10, y);    
        line(3,height/2-20,width/3-20,height/2-20);       // draw axis
        line(20,0,20,height/2-5);           // draw axis
  }
}
void labels(){
        fill(45,97,200);
        text(table.getColumnTitle(idx0),200,height/2-8);
        text(table.getColumnTitle(idx1),0,height/4);
        fill(0);
        text("BARCHART",35,30);
        fill(#CB3131);
        text(int(maxY),0,10);
        text(int(maxY/2),0,height/4-20);
        text(int(maxY/4),0,3*height/8-20);
        text(int(3*maxY/4),0,height/8-20);
        text(0,10,height/2-10);
        text(int(3*maxX/4),250,height/2-10);
        text(int(maxX/4),100,height/2-10);
        text(int(maxX),width/3-40,height/2-10);
        noFill();
}
}