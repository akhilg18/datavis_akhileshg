class histoGram extends Frame {
  
    Table data;
    float margin = 20;
    int idx0=0, idx1=1;
    float minX, maxX;
    float minY, maxY;
    histoGram( Table data ){
     this.data = data;
     minX = min(data.getFloatColumn(idx0));
     maxX = max(data.getFloatColumn(idx0));
     
     minY = min(data.getFloatColumn(idx1));
     maxY = max(data.getFloatColumn(idx1));

  }
  
void  draw() {
  int j = width/3+20;
  for (int i = 0; i < 16; i++) {
        TableRow r = table.getRow(i);
        TableRow r1 = table.getRow(i+1);
       float x = map(r.getFloat(idx1), minX, maxX,  v0 + v0+margin,2*height/3 - margin);
       float y = map(r1.getFloat(idx1), minX, maxX,  v0 + v0+margin,2*height/3 - margin);

        labels();           //function to draw labels
        stroke(0);
        line(j,height/2-x-95,j+20,height/2-y-95);
        fill(27,234,87);
        ellipse(j,height/2-x-95,6,6);
        noFill();
        j=j+20;       
          fill(203,34,234);
          line(width/3+3,height/2-20,2*width/3-20,height/2-20);   //draw axis
          line(width/3+20,0,width/3+20,height/2-5);               //draw axis
          noFill();
  }
}
void labels(){
        text(table.getColumnTitle(idx0),600,height/2-8);
        text(table.getColumnTitle(idx1),width/3,height/4);
        fill(0);
        text("LINECHART",width/3+35,30);
        fill(#CB3131);
        text(int(maxY),width/3,10);
        text(int(maxY/2),width/3,height/4-20);
        text(int(maxY/4),width/3,3*height/8-20);
        text(int(3*maxY/4),width/3,height/8-20);
        text(0,width/3+10,height/2-10);
        text(int(3*maxX/4),width/3+250,height/2-10);
        text(int(maxX/4),width/3+100,height/2-10);
        text(int(maxX),2*width/3-40,height/2-10);
        noFill();
      }
}