/*Akhilesh Gundaboina(U25998227)*/

Frame myFrame = null;
Frame myFrame1 = null;
Frame myFrame2 = null;
Frame myFrame3 = null;
Frame myFrame4 = null;


Table table;
float[] column1,column2,column3,column4;
int idx0=0,idx1=1;

void setup(){
 size(1200,800);
 for(int i=0;i<10;i++) 
 strokeWeight(5);
 line(0,height/2,width,height/2);           //margins
 line(width/3,0,width/3,height/2);          //margins
 line(2*width/3,0,2*width/3,height/2);      //margins
 line(width/2,height/2,width/2,height);     //margins
 strokeWeight(0);
 selectInput("Select a file to process:", "fileSelected");
}

void fileSelected(File selection) {
  if (selection == null) {
    println("Window was closed or the user hit cancel.");
    selectInput("Select a file to process:", "fileSelected");
  } else {
    println("User selected " + selection.getAbsolutePath());
    table = loadTable( selection.getAbsolutePath(), "header" );
    
    ArrayList<Integer> useColumns = new ArrayList<Integer>();
    for(int i = 0; i < table.getColumnCount(); i++){
      if( !Float.isNaN( table.getRow( 0 ).getFloat(i) ) ){
        println( i + " - type float" );
        useColumns.add(i);
      }
      else{
        println( i + " - type string" );
      }
    }
      myFrame = new barChart( table);
      myFrame1 = new histoGram( table );
      myFrame2 = new scatterPlot( table);
      myFrame3 = new parallelPlot( table );
      myFrame4 = new scatterMatrix( table );

    
  }
}

void draw(){
 background( 255 );
 if( table == null ) 
    return;
  if( myFrame != null ){
       myFrame.setPosition( 0, 0, width, height );
       myFrame.draw();
  }
  if( myFrame1 != null ){
       myFrame1.setPosition( 0, 0, width, height );
       myFrame1.draw();
  }
  if( myFrame2 != null ){
       myFrame2.setPosition( 0, 0, width, height );
       myFrame2.draw();
  }
  if( myFrame3 != null ){
       myFrame3.setPosition( 0, 0, width, height );
       myFrame3.draw();
  } 
  if( myFrame4 != null ){
       myFrame4.setPosition( 0, 0, width, height );
       myFrame4.draw();
  }
 line(0,height/2,width,height/2);
 line(width/3,0,width/3,height/2);
 line(2*width/3,0,2*width/3,height/2);
 line(width/2,height/2,width/2,height);
}
void mousePressed(){
  myFrame.mousePressed();
}

abstract class Frame { 
  int u0,v0,w,h;
     int clickBuffer = 2;
  void setPosition( int u0, int v0, int w, int h ){
    this.u0 = u0;
    this.v0 = v0;
    this.w = w;
    this.h = h;
  }
 abstract void draw();
  void mousePressed(){
    highlight();            //function to link views by highlighting values on each plot
  }
  
   boolean mouseInside(){
      return (u0-clickBuffer < mouseX) && (u0+w+clickBuffer)>mouseX && (v0-clickBuffer)< mouseY && (v0+h+clickBuffer)>mouseY; 
   } 

void highlight(){
  fill(100,120,230);
  rect(width/3-150,20,100,40);
  rect(2*width/3-150,20,100,40);
  rect(width-150,20,100,40);
  rect(20,height/2+20,100,40);
  rect(width/2,height/2+40,100,40); 
  fill(0);
  text(table.getColumnTitle(idx0) + " : " + mouseX,width/3-150,30);
  text(table.getColumnTitle(idx1) + " : " + mouseY,width/3-150,50);
  text(table.getColumnTitle(idx0) + " : " + mouseX,2*width/3-150,30);
  text(table.getColumnTitle(idx1) + " : " + mouseY,2*width/3-150,50);
  text(table.getColumnTitle(idx0) + " : " + mouseX,width-150,30);
  text(table.getColumnTitle(idx1) + " : " + mouseY,width-150,50);
  text(table.getColumnTitle(idx0) + " : " + mouseX,20,height/2+30);
  text(table.getColumnTitle(idx1) + " : " + mouseY,20,height/2+50);
  text(table.getColumnTitle(idx0) + " : " + mouseX,width/2+5,height/2+50);
  text(table.getColumnTitle(idx1) + " : " + mouseY,width/2+5,height/2+70);
   }
}