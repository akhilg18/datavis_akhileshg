class scatterPlot extends Frame {
  
    Table data;
    float border = 20;
    //int idx0=0, idx1=1;
    float minX, maxX;
    float minY, maxY;
  scatterPlot( Table data ){
     this.data = data;
     minX = min(data.getFloatColumn(idx0));
     maxX = max(data.getFloatColumn(idx0));
     
     minY = min(data.getFloatColumn(idx1));
     maxY = max(data.getFloatColumn(idx1));

  }
  
void  draw() {
  for (int i = 0; i < table.getRowCount(); i++) {
        TableRow r = table.getRow(i);
        float x = map(r.getFloat(idx0), minX, maxX, 2*width/3+20,width-20);   
        float y = map(r.getFloat(idx1), minY, maxY,20,height/2-20);
        labels();
        fill(170,90,80);
        noStroke();
        ellipse(x,y,5,5);
          stroke(0);
          fill(20,23,224);
          line(2*width/3+3,height/2-20,width-20,height/2-20);
          line(2*width/3+20,0,2*width/3+20,height/2-5);
          text(table.getColumnTitle(idx0),1000,height/2-8);
          text(table.getColumnTitle(idx1),2*width/3,height/4);
          fill(0);
          text("SCATTERPLOT",2*width/3+35,30);
          noFill();      
  }
}
void labels(){
        text(table.getColumnTitle(idx0),1000,height/2-8);
        text(table.getColumnTitle(idx1),2*width/3,height/4);
        fill(0);
        text("SCATTERPLOT",2*width/3+35,30);
        fill(#CB3131);
        text(int(maxY),2*width/3,10);
        text(int(maxY/2),2*width/3,height/4-20);
        text(int(maxY/4),2*width/3,3*height/8-20);
        text(int(3*maxY/4),2*width/3,height/8-20);
        text(0,2*width/3+10,height/2-10);       
        text(int(3*maxX/4),2*width/3+250,height/2-10);
        text(int(maxX/4),2*width/3+100,height/2-10);
        text(int(maxX),width-40,height/2-10);
                  noFill();      

}
}