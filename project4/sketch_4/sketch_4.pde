float[] satm,satv,act,gpa;
String filename;
String[] rawdata;
float x1,y1,y2,y3,b=137.5;
void setup()  {
  size(800,600);
  selectInput("Select a file to process", "fileSelected");           /* for a file to be selected*/
  while(filename==null){
delay(1000);}
readData();  // function to read data from CSV file.
}
void readData(){
  rawdata  = loadStrings(filename); 
  satm = new float[rawdata.length-1];
  satv = new float[rawdata.length-1];
  act = new float[rawdata.length-1];
  gpa = new float[rawdata.length-1];
  for(int i=1; i<rawdata.length; i++){
    String[] thisRow = split(rawdata[i], ",");
    satm[i-1] = float(thisRow[0]);
    satv[i-1] = float(thisRow[1]);
    act[i-1] = float(thisRow[2]);
    gpa[i-1] = float(thisRow[3]);
  }
    println(satm);
}

void fileSelected(File selection)
{ if (selection == null) 
     { println("Window was closed or the user hit cancel."); } 
   else 
     { println("User selected " + selection.getName());
     filename = selection.getName();
     }
}
 
 void draw() { 
background(255);
fill(255);
rect(610,100,180,300);
first();
sec();
third();
fourth();
label();
mouseOver();
}
/* Functions*/

void first() {                                          /* Function for first column of matrix */
for(int i=0; i<satm.length; i++)  {
 x1 = map(satm[i],min(satm),max(satm),50,50+b);
y1 = map(gpa[i],min(gpa),max(gpa),50,50+b);
y2 = map(act[i],min(act),max(act),50+b,50+b*2);
y3 = map(satv[i],min(satv),max(satv),50+b*2,50+b*3);
ellipse(x1,height-y1,1,1);
ellipse(x1,height-y2,1,1);
ellipse(x1,height-y3,1,1);
}
}

void sec() {                                            /* Function for second column of matrix */
 for(int i=0; i<satv.length; i++) {
  x1 = map(satv[i],min(satv),max(satv),50+b,50+b*2);
  y1 = map(gpa[i],min(gpa),max(gpa),50,50+b);
y2 = map(act[i],min(act),max(act),50+b,50+b*2);
y3 = map(satm[i],min(satm),max(satm),50+b*3,50+b*4);
ellipse(x1,height-y1,1,1);
ellipse(x1,height-y2,1,1);
ellipse(x1,height-y3,1,1);
   }
}

void third() {                                               /* Function for third column of matrix */
 for(int i=0; i<act.length; i++) {
  x1 = map(act[i],min(act),max(act),50+b*2,50+b*3);
  y1 = map(gpa[i],min(gpa),max(gpa),50,50+b);
y2 = map(satv[i],min(satv),max(satv),50+b*2,50+b*3);
y3 = map(satm[i],min(satm),max(satm),50+b*3,50+b*4);
ellipse(x1,height-y1,1,1);
ellipse(x1,height-y2,1,1);
ellipse(x1,height-y3,1,1);
   }
}
void fourth() {                                                 /* Function for fourth column of matrix */
 for(int i=0; i<gpa.length; i++) {
  x1 = map(gpa[i],min(gpa),max(gpa),50+b*3,50+b*4);
  y1 = map(act[i],min(act),max(act),50+b,50+b*2);
y2 = map(satv[i],min(satv),max(satv),50+b*2,50+b*3);
y3 = map(satm[i],min(satm),max(satm),50+b*3,50+b*4);
ellipse(x1,height-y1,1,1);
ellipse(x1,height-y2,1,1);
ellipse(x1,height-y3,1,1);
   }
}  
  void label() {                                          /* all labelings */
    
line(50,0,50,600);
line(0,550,600,550);
for(int i=50;i<=height;i+=137.5)  {
line(i,0,i,height-50);  
}
for(int j=0;j<=600;j+=137.5)  {
 line(50,j,600,j); 
}
   fill(0);
   int a=100;
  
 text("SATM",a,570); 
 text("SATV",a+b,570);
 text("ACT",a+(b*2),570);
 text("GPA",a+(b*3),570); 
 text("SATM",10,a);
 text("SATV",10,a+b);
 text("ACT",10,a+(b*2));
 text("GPA",10,a+(b*3));
  }
  
  void mouseOver()                                  /* when mouse is over a specific cell it shows that cell in detail view */
  {
 if((mouseX>50)&&(mouseX<50+b))
 {
   if((mouseY>412.5)&&(mouseY<550))
   {
  for(int i=0; i<act.length; i++) {
   x1 = map(satm[i],min(satm),max(satm),610,790);
y1 = map(gpa[i],min(gpa),max(gpa),187.5,490); 
//fill(255);
ellipse(x1,height-y1,1,1);
 }
   }
 }
 if((mouseX>50)&&(mouseX<50+b))
 {
   if((mouseY>412.5-b)&&(mouseY<550-b))
   {
  for(int i=0; i<act.length; i++) {
   x1 = map(satm[i],min(satm),max(satm),610,790);
y1 = map(act[i],min(act),max(act),197.5,490);
fill(255);
ellipse(x1,height-y1,1,1);
 }
   }
 }
  if((mouseX>50)&&(mouseX<50+b))
 {
   if((mouseY>412.5-(2*b))&&(mouseY<550-(2*b)))
   {
  for(int i=0; i<act.length; i++) {
   x1 = map(satm[i],min(satm),max(satm),610,790);
y1 = map(satv[i],min(satv),max(satv),197.5,490);
fill(255);
ellipse(x1,height-y1,1,1);
 }
   }
 }
 if((mouseX>50+b)&&(mouseX<50+(2*b)))
 {
   if((mouseY>412.5)&&(mouseY<550))
   {
  for(int i=0; i<act.length; i++) {
   x1 = map(satv[i],min(satv),max(satv),610,790);
y1 = map(gpa[i],min(gpa),max(gpa),197.5,490);
fill(255);
ellipse(x1,height-y1,1,1);
 }
   }
 }
 if((mouseX>50+b)&&(mouseX<50+(2*b)))
 {
   if((mouseY>412.5-b)&&(mouseY<550-b))
   {
  for(int i=0; i<act.length; i++) {
   x1 = map(satv[i],min(satv),max(satv),610,790);
y1 = map(act[i],min(act),max(act),197.5,490);
fill(255);
ellipse(x1,height-y1,1,1);
 }
   }
 }
 if((mouseX>50+b)&&(mouseX<50+(2*b)))
 {
   if((mouseY>412.5-(3*b))&&(mouseY<550-(3*b)))
   {
  for(int i=0; i<act.length; i++) {
   x1 = map(satv[i],min(satv),max(satv),610,790);
y1 = map(satm[i],min(satm),max(satm),197.5,490);
fill(255);
ellipse(x1,height-y1,1,1);
 }
   }
 }
 if((mouseX>50+(2*b))&&(mouseX<50+(3*b)))
 {
   if((mouseY>412.5-(3*b))&&(mouseY<550-(3*b)))
   {
  for(int i=0; i<act.length; i++) {
   x1 = map(act[i],min(act),max(act),610,790);
y1 = map(satm[i],min(satm),max(satm),197.5,490);
fill(255);
ellipse(x1,height-y1,1,1);
 }
   }
 }
 if((mouseX>50+(2*b))&&(mouseX<50+(3*b)))
 {
   if((mouseY>412.5-(2*b))&&(mouseY<550-(2*b)))
   {
  for(int i=0; i<act.length; i++) {
   x1 = map(act[i],min(act),max(act),610,790);
y1 = map(satv[i],min(satv),max(satv),197.5,490);
fill(255);
ellipse(x1,height-y1,1,1);
 }
   }
 }
 if((mouseX>50+(2*b))&&(mouseX<50+(3*b)))
 {
   if((mouseY>412.5)&&(mouseY<550))
   {
  for(int i=0; i<act.length; i++) {
   x1 = map(act[i],min(act),max(act),610,790);
y1 = map(gpa[i],min(gpa),max(gpa),197.5,490);
fill(255);
ellipse(x1,height-y1,1,1);
 }
   }
 }
 if((mouseX>50+(3*b))&&(mouseX<50+(4*b)))
 {
   if((mouseY>412.5-b)&&(mouseY<550-b))
   {
  for(int i=0; i<act.length; i++) {
   x1 = map(gpa[i],min(gpa),max(gpa),610,790);
y1 = map(act[i],min(act),max(act),197.5,490);
fill(255);
ellipse(x1,height-y1,1,1);
 }
   }
 }
 if((mouseX>50+(3*b))&&(mouseX<50+(4*b)))
 {
   if((mouseY>412.5-(2*b))&&(mouseY<550-(2*b)))
   {
  for(int i=0; i<act.length; i++) {
   x1 = map(gpa[i],min(gpa),max(gpa),610,790);
y1 = map(satv[i],min(satv),max(satv),197.5,490);
fill(255);
ellipse(x1,height-y1,1,1);
 }
   }
 }
  if((mouseX>50+(3*b))&&(mouseX<50+(4*b)))
 {
   if((mouseY>412.5-(3*b))&&(mouseY<550-(3*b)))
   {
  for(int i=0; i<act.length; i++) {
   x1 = map(gpa[i],min(gpa),max(gpa),610,790);
y1 = map(satm[i],min(satm),max(satm),197.5,490);
fill(255);
ellipse(x1,height-y1,1,1);
 }
   }
 }
 }
 