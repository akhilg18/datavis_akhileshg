Table table;
/* Declaring the values */
String filename;
String[] rawdata;
float[] satm,satv,act,gpa,x;
float t=2.94;
void setup()  {
  size(800,600);
  selectInput("Select a file to process", "fileSelected");   /* selecting a file to be selected*/
  while(filename==null){
delay(1000);}
readData();  // function to read data from CSV file.
}
void readData(){
  rawdata  = loadStrings(filename); 
  satm = new float[rawdata.length-1];
  satv = new float[rawdata.length-1];
  act = new float[rawdata.length-1];
  gpa = new float[rawdata.length-1];
  x = new float[rawdata.length-1];
  for(int i=1; i<rawdata.length; i++){
    String[] thisRow = split(rawdata[i], ",");
    satm[i-1] = float(thisRow[0]);
    satv[i-1] = float(thisRow[1]);
    act[i-1] = float(thisRow[2]);
    gpa[i-1] = float(thisRow[3]);
  }
}

void fileSelected(File selection)
{ if (selection == null) 
     { println("Window was closed or the user hit cancel."); } 
   else 
     { println("User selected " + selection.getName());
     filename = selection.getName();
     }
}
 
 
 void draw() { 
   background(255);

keyPressed();   /*calling the functions*/
mouseMoved();
}
void keyPressed()                                                     /*this function is done when a key is pressed*/
{
  fill(0);
  text("Press the specified key for specified value",100,50);              
  text("1 to plot barchart with SATM values",100,65);
  text("2 to plot barchart with SATV values ",100,75);
  text("3 to plot barchart with ACT values ",100,85);
  text("4 to plot barchart with GPA values ",100,95);
  if(key=='1')                                                     /*if key matches then it goes to function A */
  {
   A();
  }
  if(key=='2')                                                      /*if key matches then it goes to function B */
  {
   B();
  }
  if(key=='3')                                                         /*if key matches then it goes to function C */
  {
   C();
  }
  if(key=='4')                                                          /*if key matches then it goes to function D */
  {
   D(); 
  }
}
 void mouseMoved()  {
  fill(175);
   
  if(get(mouseX,mouseY)!=-1)   /*sees that popup window doesnot appear when mouse not on graph*/
  {
  rect(500,50,100,50);               /* rect for popup window*/
   //for(int i=0; i<rawdata.length-1; i++)
     fill(0);
    //text(satm[i]+" , "+x[i],520,70);
    text("("+mouseX+" , "+(height-mouseY)+")",520,70);
   
  }
 }

void A() {                                          /*bar chart for satm values*/
for(int i=0; i<rawdata.length-1; i++)  {
  fill(0,0,200);
  stroke(0);
rect((t)*i,height-(satm[i]/2),t,satm[i]/2);
}
}

 void B() {                                           /*bar chart for satv values*/
 for(int i=0; i<rawdata.length-1; i++) {
fill(0,200,200);
rect(t*i,height-(satv[i]/2),t,satv[i]/2);
   }
}

void C() {                                             /*bar chart for act values*/
 for(int i=0; i<rawdata.length-1; i++) {
  fill(0,200,0);
rect(t*i,height-(act[i]*10),t,act[i]*10);
   }
}
void D() {                                              /*bar chart for gpa values*/
  fill(150);
 for(int i=0; i<rawdata.length-1; i++) {
  rect(t*i,height-(gpa[i]*100),t,gpa[i]*100);
   }
}  


   

  