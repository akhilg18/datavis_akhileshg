Table table;
String filename;
String[] rawdata;
float[] satm,satv,act,gpa,x,mm,value1;
float m;
float l;
 int margin = 50;

void setup() {
  size(800, 600);
  selectInput("Select a file to process:", "fileSelected");            /* Selecting the  file*/
    while(filename==null){
delay(1000);}
readData();  // function to read data from CSV file.
 
}
void readData(){
  rawdata  = loadStrings(filename); 
  satm = new float[rawdata.length-1];
  satv = new float[rawdata.length-1];
  act = new float[rawdata.length-1];
  gpa = new float[rawdata.length-1];
  x = new float[rawdata.length-1];
  mm = new float[rawdata.length-1];
  value1 = new float[rawdata.length-1];
  for(int i=1; i<rawdata.length; i++){
    String[] thisRow = split(rawdata[i], ",");
    satm[i-1] = float(thisRow[0]);
    satv[i-1] = float(thisRow[1]);
    act[i-1] = float(thisRow[2]);
    gpa[i-1] = float(thisRow[3]);
  }
}

void fileSelected(File selection)
{ if (selection == null) 
     { println("Window was closed or the user hit cancel."); } 
   else 
     { println("User selected " + selection.getName());
     filename = selection.getName();
     }
}


void draw() {
  background(255);
  fill(0);
  line(20,600,20,0);                         /* axes lines*/
  line(0,580,800,580);
  keyPressed();
  mousePressed();
}
void keyPressed()                                                   /* happens when a key is pressed*/
{
  fill(0);
  text("Press the specified key for specified value",100,40);
  text("1 for Column1 plot",100,65);
  text("2 for Column2 plot",100,85);
  text("3 for Column3 plot",100,105);
  text("4 for Column4 plot",100,125);
  if(key=='1')                                                        /*if key matches then it goes to function A */
  {
   A();
  }
  if(key=='2')                                                        /*if key matches then it goes to function B */
  {
   B();
  }
  if(key=='3')                                                         /*if key matches then it goes to function C */
  {
   C();
  }
  if(key=='4')                                                        /*if key matches then it goes to function D */
  {
   D(); 
  }
}
 void mousePressed()  {                                             /* happens when mouse is pressed */
  fill(175);
   
  if(get(mouseX,mouseY)!=-1)                      /*sees that popup window doesnot appear when mouse not on graph*/
  {
  rect(500,20,150,100);
   for(int i=0; i<rawdata.length-1; i++)                /* rect for popup window*/
   {
     fill(0);
      text(mouseX+" , "+mouseY,520,50);
   
   }
  }
 }

void A()
{
  
for(int i=0; i<rawdata.length-1; i++) {
    stroke(75);
  fill(220,200,0);
  l = 2.64;
m = map(satm[i], min(satm),max(satm) , 0, 400);
 mm[i]=m;
 ellipse(margin+(l*i),((height - margin) - m), 1,1);
if(i>0) {
 line((margin + (l*i)), ((height-margin)-mm[i]), margin + l*(i-1),((height-margin)-mm[i-1])); 
 }
}
}
void B()
{
  
for(int i=0; i<rawdata.length-1; i++) {
    stroke(75);
  fill(220,200,0);
  l = 2.64;
m = map(satv[i], min(satv),max(satv) , 0, 400);
 mm[i]=m;
 ellipse(margin+(l*i),((height - margin) - m), 1,1);
if(i>0) {
 line((margin + (l*i)), ((height-margin)-mm[i]), margin + l*(i-1),((height-margin)-mm[i-1])); 
 }
}
}
void C()
{
  
for(int i=0; i<rawdata.length-1; i++) {
    stroke(75);
  fill(220,200,0);
  l = 2.64;
m = map(act[i], min(act),max(act) , 0, 400);
 mm[i]=m;
 ellipse(margin+(l*i),((height - margin) - m), 1,1);
if(i>0) {
 line((margin + (l*i)), ((height-margin)-mm[i]), margin + l*(i-1),((height-margin)-mm[i-1])); 
 }
}
}
void D()
{
  
for(int i=0; i<rawdata.length-1; i++) {
    stroke(75);
  fill(220,200,0);
  l = 2.64;
m = map(gpa[i], min(gpa),max(gpa) , 0, 400);
 mm[i]=m;
 ellipse(margin+(l*i),((height - margin) - m), 1,1);
if(i>0) {
 line((margin + (l*i)), ((height-margin)-mm[i]), margin + l*(i-1),((height-margin)-mm[i-1])); 
 }
}
}
 