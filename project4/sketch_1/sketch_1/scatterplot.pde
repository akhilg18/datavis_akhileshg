class Scatterplot
 {
   Table data;
   int xcol,ycol,margin=120,newheight,i=0,j=0;
   float[] xdata,ydata;
   String xtitle,ytitle;
   float n,xspacer,xmin,xmax,ymin,ymax;
   PVector[] positions;
  Scatterplot(Table data,int xcol,int ycol) {
    this.data=data;this.xcol=xcol;this.ycol=ycol;
    int n=data.getRowCount();
    ydata=new float[n];
    xdata=new float[n];
    positions=new PVector[n];
     for (TableRow row : data.rows()) {
          xdata[i]=row.getFloat(xcol);
          ydata[i]=row.getFloat(ycol);
          i++;
     } 
       xmin= min(xdata);
       xmax=max(xdata);
       ymax=max(ydata);
       ymin=min(ydata);
newheight=(height-margin)-margin;
 
for(int z=0;z<data.getRowCount();z++) {
  float yadjscore=map(ydata[z],ymin,ymax,0,newheight);
  float ypos=height-margin-yadjscore;
  float xadjscore=map(xdata[z],xmin,xmax,0,width-margin-margin);
  float xpos=margin+xadjscore;
  positions[z]=new PVector(xpos,ypos);
}
  } 
  void draw() {
   for (i=0;i<data.getRowCount();i++) {
   ellipse(positions[i].x,positions[i].y,4,4);
   }  
  stroke(0);
  line(margin,height-margin,width-margin,height-margin);
  line(margin,height-margin,margin,margin);
   labels();
  }
  void labels()
  {
    int A=(height-2*margin)/10;
  int B=(width-2*margin)/10;
  float xnorm=(xmax-xmin)/10;
  float ynorm=(ymax-ymin)/10;
  for(int j=0;j<10;j++)
  {
    noStroke();
   fill(125,126,255);
    textSize(8);
    text(String.format("%.2f",ymin+(ynorm*j)),margin-50,(height-margin)-(A*j));
  }
for(int k=0;k<10;k++) {  
  noStroke();
  fill(0,126,255);
  textSize(8);
  text(String.format("%.2f",xmin+(xnorm*k)),margin+(B*k),(height-margin+25));   
}
fill(130,30,200); textSize(20);
xtitle=data.getColumnTitle(xcol);
ytitle=data.getColumnTitle(ycol);
text(xtitle +"  Vs  "+ytitle,width/2,margin/2);
textSize(15);
text(xtitle,width/2,550); 
text(ytitle,15,height/2+25); 
  }
 }
 
   