Table data;

ArrayList<Integer> useColumns;
ArrayList<Integer> input= new ArrayList<Integer>();

int xcol,ycol,i=0;
Scatterplot sp;
boolean newinput=true;
void setup()
  { 
  size(800, 600);
  background(255);
  selectInput("Select a file to process:", "fileSelected"); 
  while(data==null)
  {
    delay(1000);
  }
  fill(0);
   text("ON X-Axis,enter column numbers[0,1,2,3......n-1]",100,50);
   noFill();
 }
 void fileSelected(File selection) {
  if (selection == null) {
    println("Window was closed or the user hit cancel.");
    //selectInput("Select a file to process:", "fileSelected");
  } else {
    println("User selected " + selection.getAbsolutePath());
    data = loadTable( selection.getAbsolutePath(), "header" );
    
    ArrayList<Integer> useColumns = new ArrayList<Integer>();
    for(int i = 0; i < data.getColumnCount(); i++){
      if( !Float.isNaN( data.getRow( 0 ).getFloat(i) ) ){
        println( i + " - type float" );
        useColumns.add(i);
      }
      else{
        println( i + " - type string" );
      }
    }
  }
 }
 
 
  void draw()
 {
   if(input.size()==2)
   {
     int xcol=input.get(0);
     int ycol=input.get(1);
     sp=new Scatterplot(data,xcol,ycol);
     sp.draw();
   } 
 }
 void keyPressed()
 {
   if (input.size()==0)
   {
       fill(0);
        text("ON Y-Axis,enter column numbers[0,1,2,3......n-1]",100,70);
       noFill();
 }
   if(input.size()<=2)
   {
   int numericValue = Character.getNumericValue(key);
   println(numericValue);
  input.add(numericValue);
  newinput=true;
   }
  if(input.size()>=2)
  {
    newinput=false;
  }
 }
 
void mouseMoved()  {                                                  /* happens if a mouse is pressed */
  fill(175);
  if(get(mouseX,mouseY)!=-1)                                           /*sees that popup window doesnot appear when mouse not on graph*/
  {
     rect(630,300,130,100);                                              /* rect for popup window*/
     fill(0);
     text(mouseX+" , "+(height-mouseY),650,350);
  }
 }

 
 
 
 