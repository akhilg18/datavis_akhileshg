/*program to draw a scatterplot matrix from a CSV file*/
/*Akhilesh Gundaboina(U25998227)*/

String filename;
String[] rawdata;
float[] satm,satv,act,gpa;
int margin;
float maxsatm,minsatm,maxsatv,minsatv,maxact,minact,maxgpa,mingpa;
void setup()
{
size(800,600); 
background(255);
fill(200);
stroke(3);
selectInput("Select a file to process:", "fileSelected");
while(filename==null){
delay(1000);}
readData();  // function to read data from CSV file.
margin=40;
fill(50,79,98);
    stroke(10);
    line(margin,margin,width-margin,margin);       //draw x-axis
    line(margin,height-margin,width-margin,height-margin);  
    stroke(5);
    line(margin,((height-2*margin)/4)+margin,width-margin,((height-2*margin)/4)+margin);
    line(margin,2*((height-2*margin)/4)+margin,width-margin,2*((height-2*margin)/4)+margin);
    line(margin,3*((height-2*margin)/4)+margin,width-margin,3*((height-2*margin)/4)+margin);
    text("SATM",width/7,20);
    text("SATV",width/3,20);
    text("ACT",width-350,20);
    text("GPA",650,20);

    stroke(10);
    line(margin,margin,margin,height-margin);           //draw y-axis
    line(width-margin,margin,width-margin,height-margin);           
    stroke(5);
    line(((width-2*margin)/4)+margin,margin,((width-2*margin)/4)+margin,height-margin);
    line(2*((width-2*margin)/4)+margin,margin,2*((width-2*margin)/4)+margin,height-margin);
    line(3*((width-2*margin)/4)+margin,margin,3*((width-2*margin)/4)+margin,height-margin);
    text("SATM",0,height/5);
    text("SATV",0,250);
    text("ACT",0,380);
    text("GPA",0,500);

  maxsatm=max(satm);
  minsatm=min(satm);
  maxsatv=max(satv);
  minsatv=min(satv);
  
  maxact=max(act);
  minact=min(act);
  maxgpa=max(gpa);
  mingpa=min(gpa);
}

  
  
void draw(){ 
  noStroke();
    for(int i=0; i<rawdata.length-1; i++){
      //print(satm[i]);
      fill(20,40,65);
      float X1=map(satm[i],maxsatm,minsatm,margin,220);
      float X2=map(satv[i],maxsatv,minsatv,220,400);
      float X3=map(act[i],maxact,minact,400,580);
      float X4=map(gpa[i],maxgpa,mingpa,580,width-margin);
      
      float Y1=map(satm[i],maxsatm,minsatm,margin,170);  
      float Y2=map(satv[i],maxsatv,minsatv,170,300);  
      float Y3=map(act[i],maxact,minact,300,430);  
      float Y4=map(gpa[i],maxgpa,mingpa,430,height-margin);  

      fill(#F7F714);
      ellipse(X2,Y1,2,2);    //plot between SATV,SATM
      fill(#F314F7);
      ellipse(X3,Y1,2,2);    //plot betweem ACT,SATM
      fill(#14D7F7);
      ellipse(X4,Y1,2,2);    //plot betweem GPA,SATM
      
      fill(#14F737);
      ellipse(X1,Y2,2,2);    //plot betweem SATM,SATV
      fill(#F7143E);
      ellipse(X3,Y2,2,2);    //plot betweem ACT,SATV
      fill(#F77714);
      ellipse(X4,Y2,2,2);    //plot betweem GPA,SATV
      fill(#8C14F7);
      ellipse(X1,Y3,2,2);    //plot betweem SATM,ACT
      fill(#D8F714);
      ellipse(X2,Y3,2,2);    //plot betweem SATV,ACT
      fill(#C3CCD8);
      ellipse(X4,Y3,2,2);    //plot betweem GPA,ACT
      fill(#FC8A97);
      ellipse(X1,Y4,2,2);    //plot betweem SATM,GPA
      fill(#2D550E);
      ellipse(X2,Y4,2,2);    //plot betweem SATV,GPA
      fill(#0F0101);
      ellipse(X3,Y4,2,2);    //plot betweem ACT,GPA
      noFill();
    }
}
    
    
void readData(){
  rawdata  = loadStrings(filename); 
  satm = new float[rawdata.length-1];
  satv = new float[rawdata.length-1];
  act = new float[rawdata.length-1];
  gpa = new float[rawdata.length-1];
  for(int i=1; i<rawdata.length; i++){
    String[] thisRow = split(rawdata[i], ",");
    satm[i-1] = float(thisRow[0]);
    satv[i-1] = float(thisRow[1]);
    act[i-1] = float(thisRow[2]);
    gpa[i-1] = float(thisRow[3]);
  }
    println(satm);
}

void fileSelected(File selection)
{ if (selection == null) 
     { println("Window was closed or the user hit cancel."); } 
   else 
     { println("User selected " + selection.getName());
     filename = selection.getName();
     }
}