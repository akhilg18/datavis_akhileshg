/*program to draw a scatterplot with ACT on X-axis and GPA on Y-axis using data from a CSV file*/
/*Akhilesh Gundaboina(U25998227)*/

String filename;
String[] rawdata;
int margin;
float[] satm,satv,act,gpa;
float maxact,minact,maxgpa,mingpa;
void setup()
{
size(800,600); 
background(#B8FAF8);
fill(200);
stroke(3);
selectInput("Select a file to process:", "fileSelected");
while(filename==null){
delay(1000);}
readData();  // function to read data from CSV file.
margin=40;
fill(#FF0A1F);
    line(0,margin,width-margin,margin);       //draw x-axis
           text("ACT",width/2,20);
    line(margin,0,margin,height-margin);           //draw y-axis
           text("GPA",0,height/2);
  maxact=max(act);
  minact=min(act);
  maxgpa=max(gpa);
  mingpa=min(gpa);
}

  
  
void draw(){ 
    for(int i=0; i<rawdata.length-1; i++){
      float X=map(act[i],maxact,minact,margin,width-margin);
      float Y=map(gpa[i],maxgpa,mingpa,margin,height-margin);
      fill(#456463);
      noStroke();
      ellipse(X,Y,4,4);
    }
}
    
    
void readData(){
  rawdata  = loadStrings(filename); 
  satm = new float[rawdata.length-1];
  satv = new float[rawdata.length-1];
  act = new float[rawdata.length-1];
  gpa = new float[rawdata.length-1];
  for(int i=1; i<rawdata.length; i++){
    String[] thisRow = split(rawdata[i], ",");
    satm[i-1] = float(thisRow[0]);
    satv[i-1] = float(thisRow[1]);
    act[i-1] = float(thisRow[2]);
    gpa[i-1] = float(thisRow[3]);
  }
  println(act);
}

void fileSelected(File selection)
{ if (selection == null) 
     { println("Window was closed or the user hit cancel."); } 
   else 
     { println("User selected " + selection.getName());
     filename = selection.getName();
     }
}