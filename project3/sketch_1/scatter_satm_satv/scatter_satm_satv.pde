/*program to draw a scatterplot with SATM on X-axis and SATV on Y-axis using data from a CSV file*/
/*Akhilesh Gundaboina(U25998227)*/

String filename;
String[] rawdata;
int margin;
float[] satm,satv,act,gpa;
float maxsatm,minsatm,maxsatv,minsatv;
void setup()
{
size(800,600); 
background(#D9D5E0);
stroke(3);
selectInput("Select a file to process:", "fileSelected");
while(filename==null){
delay(1000);}
readData();  // function to read data from CSV file.
margin = 40;
fill(#FA0828);
    line(0,margin,width-margin,margin);       //draw x-axis
           text("SATM",width/2,20);
    line(margin,0,margin,height-margin);           //draw y-axis
           text("SATV",0,height/2);
  maxsatm=max(satm);
  minsatm=min(satm);
  maxsatv=max(satv);
  minsatv=min(satv);
}

  
  
void draw(){ 
  noStroke();
    for(int i=0; i<rawdata.length-1; i++){
      fill(20,40,65);
      float X=map(satm[i],maxsatm,minsatm,margin,width-margin);
      fill(120,140,160);
      float Y=map(satv[i],maxsatv,minsatv,margin,height-margin);
      fill(#6111F7);
      noStroke();
      ellipse(X,Y,4,4);
    }
}
    
    
void readData(){
  rawdata  = loadStrings(filename); 
  satm = new float[rawdata.length-1];
  satv = new float[rawdata.length-1];
  act = new float[rawdata.length-1];
  gpa = new float[rawdata.length-1];
  for(int i=1; i<rawdata.length; i++){
    String[] thisRow = split(rawdata[i], ",");
    satm[i-1] = float(thisRow[0]);
    satv[i-1] = float(thisRow[1]);
    act[i-1] = float(thisRow[2]);
    gpa[i-1] = float(thisRow[3]);
  }
  println(satm);
}

void fileSelected(File selection)
{ if (selection == null) 
     { println("Window was closed or the user hit cancel."); } 
   else 
     { println("User selected " + selection.getName());
     filename = selection.getName();
     }
}