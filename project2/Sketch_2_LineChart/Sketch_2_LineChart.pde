/*program to draw a linechart using data from a CSV file*/
/*Akhilesh Gundaboina(U25998227)*/

String filename;
String[] rawdata;
int[] year = new int[255];
int[] Value0 = new int[255];
int[] Value1 = new int[255];
String[] party = new String[255];

void setup()
{
size(600,600); 
background(255);
fill(200);
stroke(2);
selectInput("Select a file to process:", "fileSelected");
while(filename==null){
delay(1000);}
readData();              // function to read data from file
}
  
  
void draw(){ 
    line(0,560,600,560);  //draw x-axis
    for(int k=80,i=0; k<=width; k=k+40,i++){
        line(k,width-40,k,width-40+5);
        fill(0, 102, 153);
        text(year[i],k-10,width-40+15);
        fill(100, 12, 153);
        text("YEARS--->",width/2,width-40+30);
    }
    line(40,0,40,600);  //draw y-axis
    for(int k=50,i=10; k<=height; k=k+50,i=i+10){
        line(35,height-k-40,40,height-k-40);
        fill(0, 102, 153);
        text(i,20,height-40-k);
        fill(100, 12, 153);
        text("VALUE",0,height/2);
     }

   int j=40;  //margin
   for(int i=0; i<rawdata.length-1; i++){
       int t=5;   //scaling by 5.
       j=j+40;
       fill(170, 102, 153);
       ellipse(j,height-(Value1[i]*t)-40,5,5);   //plot points of value1
       text(Value1[i],j+5,height-(Value1[i]*t)-40);
   if(i<rawdata.length-2) {
      line(j,height-(Value1[i]*t)-40,j+40,height-(Value1[i+1]*t)-40); //line joining the points
      }
    } 
 }
    
    
void readData(){
  rawdata  = loadStrings(filename);
  for(int i=1; i<rawdata.length; i++){
    String[] thisRow = split(rawdata[i], ",");
    year[i-1] = int(thisRow[0]);
    Value0[i-1] = int(thisRow[1]);
    Value1[i-1] = int(thisRow[2]);
    party[i-1] = thisRow[3];
  }
}

void fileSelected(File selection)
{ if (selection == null) 
     { println("Window was closed or the user hit cancel."); } 
   else 
     { println("User selected " + selection.getName());
     filename = selection.getName();
     }
}