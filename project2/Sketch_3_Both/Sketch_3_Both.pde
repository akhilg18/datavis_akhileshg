/*program to draw both barchart and linechart using data from a CSV file using classes*/
/*Akhilesh Gundaboina(U25998227)*/

frame myFrame = null;
frame myFrame2 = null;

String filename;
String[] rawdata;
int[] year = new int[255];
int[] Value0 = new int[255];
int[] Value1 = new int[255];
String[] party = new String[255];


void setup(){
  size(600,600); 
  background(255);
fill(200);
stroke(3);
selectInput("Select a file to process:", "fileSelected");
while(filename==null){
delay(1000);}
  myFrame  = new lineChart();           
  noFill();
  myFrame2 = new barChart();
  readData(); 
  
  
}

void draw(){
  background( 255 );
  
  if( myFrame != null ){
     myFrame.draw();
  }
  if( myFrame2 != null ){
     myFrame2.draw();
  }
}



abstract class frame {
  abstract void draw();
  
}

void readData(){
rawdata  = loadStrings(filename);
  for(int i=1; i<rawdata.length; i++){
    String[] thisRow = split(rawdata[i], ",");
    year[i-1] = int(thisRow[0]);
    Value0[i-1] = int(thisRow[1]);
    Value1[i-1] = int(thisRow[2]);
    party[i-1] = thisRow[3];
  }
}

void fileSelected(File selection)
{ if (selection == null) 
     { println("Window was closed or the user hit cancel."); } 
   else 
     { println("User selected " + selection.getName());
     filename = selection.getName();
     }
}