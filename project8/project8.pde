

Frame myFrame = null;
JSONObject json;
//jsonArray[] jsonArray;
GraphVertex[] nodes;
GraphEdge[] edges;
void setup() {
  size(800, 800);  
  selectInput("Select a file to process:", "fileSelected");
}

void fileSelected(File selection) {
  if (selection == null) {
    println("Window was closed or the user hit cancel.");
    selectInput("Select a file to process:", "fileSelected");
  } 
  else {
    println("User selected " + selection.getAbsolutePath());

    ArrayList<GraphVertex> verts = new ArrayList<GraphVertex>();
    ArrayList<GraphEdge>   edges = new ArrayList<GraphEdge>();

    // TODO: PUT CODE IN TO LOAD THE GRAPH
        //   json = loadJSONObject("miserables.json");
       json = loadJSONObject(selection.getAbsolutePath());
    JSONArray nodesArray = json.getJSONArray("nodes");
    nodes = new GraphVertex[nodesArray.size()];
    ArrayList<String> nodevalue= new ArrayList<String>();
     for(int i=0; i< nodesArray.size();i++){
       JSONObject node = nodesArray.getJSONObject(i);
       String id = node.getString("id");
       int group = node.getInt("group");
       nodevalue.add(id);
        verts.add(new GraphVertex(id,group,random(40,800-40),random(40,800-40)));
       println(random(0,800));
     }
    JSONArray edgesArray = json.getJSONArray("links");
    // edges = new GraphEdge[edgesArray.size()];
     for(int i=0; i< edgesArray.size();i++){
       JSONObject edge = edgesArray.getJSONObject(i);
       String source = edge.getString("source");
       String target = edge.getString("target");
       int src=nodevalue.indexOf(source);
       int tar=nodevalue.indexOf(target);
       float value = edge.getFloat("value");
       edges.add (new GraphEdge(verts.get(src),verts.get(tar),value));
     }
    myFrame = new ForceDirectedLayout( verts, edges );
  }
}


void draw() {
  background( 255 );

  if ( myFrame != null ) {
    myFrame.setPosition( 0, 0, width, height );
    myFrame.draw();
  }
}

void mousePressed() {
  myFrame.mousePressed();
}

void mouseReleased() {
  myFrame.mouseReleased();
}

void mouseDragged() {
  myFrame.mouseDragged(); 
}

abstract class Frame {

  int u0, v0, w, h;
  int clickBuffer = 2;
  void setPosition( int u0, int v0, int w, int h ) {
    this.u0 = u0;
    this.v0 = v0;
    this.w = w;
    this.h = h;
  }

  abstract void draw();
  
  void mousePressed() { }
  void mouseReleased() { }
  void mouseDragged() { }

  boolean mouseInside() {
    return (u0-clickBuffer < mouseX) && (u0+w+clickBuffer)>mouseX && (v0-clickBuffer)< mouseY && (v0+h+clickBuffer)>mouseY;
  }
  
}