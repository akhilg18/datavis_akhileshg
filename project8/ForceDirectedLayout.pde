// most modification should occur in this file


class ForceDirectedLayout extends Frame {
  
  
  float RESTING_LENGTH = 10.0f;   // update this value
  float SPRING_SCALE   = 0.0075f; // update this value
  float REPULSE_SCALE  = 400.0f;  // update this value

  float TIME_STEP      = 0.5f;    // probably don't need to update this

  // Storage for the graph
  ArrayList<GraphVertex> verts;
  ArrayList<GraphEdge> edges;

  // Storage for the node selected using the mouse (you 
  // will need to set this variable and use it) 
  
  GraphVertex selected = null;
  

  ForceDirectedLayout( ArrayList<GraphVertex> _verts, ArrayList<GraphEdge> _edges ) {
    verts = _verts;
    edges = _edges;
  }

  void applyRepulsiveForce( GraphVertex v0, GraphVertex v1 ) {
    // TODO: PUT CODE IN HERE TO CALCULATE (AND APPLY) A REPULSIVE FORCE
    
     float forceX = 0.0,forceY = 0.0;
     float dx = v0.pos.x - v1.pos.x;
     float dy = v0.pos.y - v1.pos.y;
     float toldist = sqrt((dx*dx)+(dy*dy));
     float unitdistX = dx/toldist;
     float unitdistY = dy/toldist;
     
     float ForceX = (REPULSE_SCALE*(v1.mass*v0.mass))/pow(toldist,2)*unitdistX;
     float ForceY = (REPULSE_SCALE*(v1.mass*v0.mass))/pow(toldist,2)*unitdistY;
     
       v0.addForce(ForceX,ForceY);
       v1.addForce(-ForceX,-ForceY);
    // v0.addForce( ... );
    // v1.addForce( ... );
  }

  void applySpringForce( GraphEdge edge ) {
    // TODO: PUT CODE IN HERE TO CALCULATE (AND APPLY) A SPRING FORCE
    float forceX = 0.0,forceY = 0.0;
     float dx = edge.v0.pos.x - edge.v1.pos.x;
     float dy = edge.v0.pos.y - edge.v1.pos.y;
     float toldist = sqrt((dx*dx)+(dy*dy));
     float unitdistX = dx/toldist;
     float unitdistY = dy/toldist;
     
     float springForceX = -1*SPRING_SCALE*(toldist - RESTING_LENGTH)*unitdistX;
     float springForceY = -1*SPRING_SCALE*(toldist - RESTING_LENGTH)*unitdistY;
     
       edge.v0.addForce(springForceX,springForceY);
       edge.v1.addForce(-springForceX,-springForceY);
    
    // edge.v0.addForce( ... );
    // edge.v1.addForce( ... );
  }

  void draw() {
    update(); // don't modify this line
  textSize(20);
  fill(34,234,33);
  text("FORCE DIRECTED GRAPH",width/2-80,40);
       fill(0);
       textSize(10);
       text("Drag mouse over the plot to see resting length,spring scale and repulsive scale",2,20);
      fill(10,203,39);  rect(250-180,40,20,20); text("1",250-175,40);
      fill(29,38,234);  rect(270-180,40,20,20); text("2",270-175,40);
      fill(240,38,244);  rect(290-180,40,20,20); text("3",290-175,40);
      fill(49,67,134);  rect(310-180,40,20,20); text("4",310-175,40);
      fill(141,84,244);  rect(330-180,40,20,20); text("5",330-175,40);
      fill(#05FFF9);  rect(350-180,40,20,20); text("6",350-175,40);
      fill(#868E9B);  rect(370-180,40,20,20);text("7",370-175,40);
      fill(229,138,34);  rect(390-180,40,20,20);text("8",390-175,40);
      fill(93,12,24);  rect(410-180,40,20,20);text("9",410-175,40);
      fill(129,38,124);  rect(430-180,40,20,20);text("10",430-175,40);
      fill(0);
      text("Vertex Group",2,50);
 
    // TODO: ADD CODE TO DRAW THE GRAPH
        for(int i=0; i< verts.size();i++){
            int temp = verts.get(i).group;
            if (temp == 1)
              fill(10,203,39);
              else  if(temp == 2)
              fill(29,38,234);
              else  if(temp == 3)
              fill(240,38,244);
              else  if(temp == 4)
              fill(49,67,134);
              else  if(temp == 5)
              fill(141,84,244);
              else  if(temp == 6)
              fill(#05FFF9);
              else  if(temp == 7)
              fill(#868E9B);
              else  if(temp == 8)
              fill(229,138,34);
              else  if(temp == 9)
              fill(93,12,24);
              else  if(temp == 10)
              fill(129,38,124);
              else 
              fill(#E4E821);
            ellipse(verts.get(i).pos.x,verts.get(i).pos.y,10,10);
            noFill();
    }
    
    for(int i=0;i< edges.size();i++){
      float stroke123 = edges.get(i).weight;
      strokeWeight(stroke123/15);
     line(edges.get(i).v0.pos.x,edges.get(i).v0.pos.y,edges.get(i).v1.pos.x,edges.get(i).v1.pos.y);
    }
  }


  void mousePressed() { 
    // TODO: ADD SOME INTERACTION CODE
  }

  void mouseReleased() {    
    // TODO: ADD SOME INTERACTION CODE

  }

  void mouseDragged() {
    if ((mouseX>80 && mouseX<width-80) && (mouseY>80 && mouseY<width-80)){
    fill(#EA1111);
    textSize(12);
    text("RESTING LENGTH="+ RESTING_LENGTH + "Units",width-250,80);
    text("SPRING SCALE="+ SPRING_SCALE + "Units",width-250,100);
    text("REPULSE SCALE="+ REPULSE_SCALE + "Units",width-250,120);
     noFill();  
    }
}


  // The following function applies forces to all of the nodes. 
  // This code does not need to be edited to complete this 
  // project (and I recommend against modifying it).
  void update() {
    for ( GraphVertex v : verts ) {
      v.clearForce();
    }

    for ( GraphVertex v0 : verts ) {
      for ( GraphVertex v1 : verts ) {
        if ( v0 != v1 ) applyRepulsiveForce( v0, v1 );
      }
    }

    for ( GraphEdge e : edges ) {
      applySpringForce( e );
    }

    for ( GraphVertex v : verts ) {
      v.updatePosition( TIME_STEP );
    }
  }
}