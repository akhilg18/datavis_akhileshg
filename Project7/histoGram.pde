class histoGram extends Frame {
  
    Table data;
    float margin = 20;
    int idx0, idx1=1;
    float minX, maxX;
    float minY, maxY;
    int k=8;   //no of bins
    float temp[] = new float[k];
    float bin;
    float sum=0,mean=0,std=0;
    histoGram( Table data ){
     this.data = data;     
     hist(idx0);
  }
  
void  draw() {

       labels();           //function to draw labels
       fill(0);
               text(table.getColumnTitle(idx0),600,height/2);
       text("click on plot to swap attributes",width/3+50,10);
       text("and drag mouse to see Mean, STD, No of elements in each bin.",2*width/3-350,20);
                    for (int i = 0; i < table.getRowCount(); i++) {
       line(width/3+3,height/2-20,2*width/3-20,height/2-20);   //draw axis
          line(width/3+20,0,width/3+20,height/2-5);               //draw axis
                    }

       for(int i=0;i<k;i++){
        // fill(#45E54C);
        fill(#DFFA38);
             rect(width/3+20+i*45,height/2-20,45,-temp[i]*4);
          }
}
void labels(){
        text(table.getColumnTitle(idx0),600,height/2-8);
        text(table.getColumnTitle(idx1),width/3,height/4);
        fill(0);
        text("HISTOGRAM",width/3+35,30);
        fill(#CB3131);
        int p=0;
        for(int i=0;i<=360;i=i+45,p=p+10){
          text(p,width/3+20+i,height/2-10);  // to draw x-axis.
        }
        int q=0;
        for(int i=0;i<=315;i=i+45,q=q+10){
          text(q,width/3+3,height/2-10-i);  // to draw y-axis.
          ellipse(width/3+20,height/2-13-i,3,3);
        }         
        noFill();
      }
      
      void hist(int idx0){
        fill(45,78,65);
        text(table.getColumnTitle(idx0),600,height/2);
        minX = min(data.getFloatColumn(idx0));
        maxX = max(data.getFloatColumn(idx0));
     
        //minY = min(data.getFloatColumn(idx1));
        //maxY = max(data.getFloatColumn(idx1));
        bin=0; 
        for(int z=0;z<k;z++){
             temp[z]=0;
                  }
             for (int i = 0; i < table.getRowCount(); i++) {
                TableRow r = table.getRow(i);
                bin = floor(k*(r.getFloat(idx0)-minX)/(maxX-minX));
                for(int z=0;z<k;z++){
                  if(bin == z){
                  temp[z]=temp[z]+1;
                  }
               }
            }
            sum=0;mean=0;std=0;
  for (int i = 0; i < table.getRowCount(); i++) {
        TableRow r = table.getRow(i);
        sum = sum + r.getFloat(idx0);
  }
      mean = sum/table.getRowCount(); println("xmean="+ mean);
    for (int i = 0; i < table.getRowCount(); i++) {
        TableRow r = table.getRow(i);
        float x1 =r.getFloat(idx0);
      std = (std+((x1-mean)*(x1-mean)));
    }
    std = sqrt((std/table.getRowCount()));   println("xstd="+ std);
      }
      void mouseDragged(){
       if (mouseX>width/3 && mouseX<2*width/3){
         if(mouseY>20 && mouseY<height/2-20){
       for(int i=0;i<k;i++){
                      text(int(temp[i]),width/3+40+i*45,height/12);
           }   
           fill(#F21F0C);
           text("MEAN= "+ mean,width/3+80,height/12-20); 
           text("STD Deviation= "+ std,width/3+200,height/12-20); 
           noFill();
         }}
      }
      
      void mousePressed(){
        if (mouseX>width/3 && mouseX<2*width/3){
         if(mouseY>20 && mouseY<height/2-20){
           idx0++;
         }}
         if (idx0>3){
           idx0=0;
         }
         hist(idx0);
      }
      void keyPressed(){
        if(key==1){
          hist(1);
        } 
        else if(key==2){
          hist(2);
        }
        else if(key==3){
          hist(3);
        }
      }
}