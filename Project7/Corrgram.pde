class corrGram extends Frame {
  
    Table data;
    float border = 20;
    int idx0, idx1;
    float minX1, maxX1;
    float minY1, maxY1;
    float c1=0,c2=0,c3=0,c4=0,c5=0,c6=0;
    float s1=0,s2=0,s3=0,s4=0,s5=0,s6=0;
    float[][] temp;
        float[] rank = new float[table.getRowCount()];
        float[] rank1 = new float[table.getRowCount()];
    corrGram( Table data){
     this.data = data;
     minX1 = min(data.getFloatColumn(idx0));
     maxX1 = max(data.getFloatColumn(idx0));
     
     minY1 = min(data.getFloatColumn(idx1));
     maxY1= max(data.getFloatColumn(idx1));
   c1 = corr(0,1);   s1 = corr1(0,1);          //coor() is the function to calculate pearson coefficient.
   c2 = corr(0,2);   s2 = corr1(0,2);          //coor1() is the function to calculate spearmam's coefficient.
   c3 = corr(0,3);   s3 = corr1(0,3);
   c4 = corr(1,2);   s4 = corr1(1,2);
   c5 = corr(1,3);   s5 = corr1(1,3);
   c6 = corr(2,3);   s6 = corr1(2,3);

    }
void  draw() {
    labels();
    //for(int i=0;i<=100;i=i+20){      //legend
      text("min",250,10);
      text("max",350,10);
      fill(#FC0D0D);
      ellipse(22,height/2-10,10,10);
      text("Pearson Correlation Coefficient",30,height/2-5);
      fill(#24201D);
      ellipse(212,height/2-10,10,10);
      noFill();
      text("Spearman's rank corr coefficient",220,height/2-5);
      fill(#C1BEF7);  rect(250,10,20,20/2);
      fill(#9792E5);  rect(270,10,20,20/2);
      fill(#6D67CB);  rect(290,10,20,20/2);
      fill(#4A43AF);  rect(310,10,20,20/2);
      fill(#261F8E);  rect(330,10,20,20/2);
      fill(#08016C);  rect(350,10,20,20/2);
      
      fill(#F1FAF0);  rect(250,21,20,20/2);
      fill(#BBE5B7);  rect(270,21,20,20/2);
      fill(#6FBC68);  rect(290,21,20,20/2);
      fill(#48A041);  rect(310,21,20,20/2);
      fill(#2B8623);  rect(330,21,20,20/2);
      fill(#0C5506);  rect(350,21,20,20/2);
    fill(255);
    rect(3*border,2*border,80,80);
    fill(#4A43AF);   //lightest
    //rect(3*border+80,2*border,80,80);
    fill(#48A041); ellipse(3*border+80+40,2*border+40,75,75);
    fill(#FC0D0D); text(c1,3*border+10+80,2*border+35);
    fill(#24201D); text(s1,3*border+10+80,2*border+55);
    fill(#261F8E);   //lighter
    //rect(3*border+2*80,2*border,80,80);
    fill(#2B8623);  ellipse(3*border+2*80+40,2*border+40,80,80);
    fill(#FC0D0D); text(c2,3*border+10+2*80,2*border+35);
        fill(#24201D); text(s2,3*border+10+2*80,2*border+55);
    fill(#C1BEF7);  // light   fill(#ED1F37)   //dark   fill(#F0020A);  //darkest.
    //rect(3*border+3*80,2*border,80,80);
        fill(#F1FAF0); ellipse(3*border+3*80+40,2*border+40,60,60);
    fill(#FC0D0D); text(c3,3*border+10+3*80,2*border+35);
    fill(#24201D); text(s3,3*border+10+3*80,2*border+55);

    
        fill(#4A43AF);  // light   fill(#ED1F37)   //dark   fill(#F0020A);  //darkest.
    rect(3*border,2*border+80,80,80);
        fill(#24201D); text(s1,3*border+10,2*border+80+35);
                fill(#FC0D0D); text(c1,3*border+10,2*border+80+55);
    fill(255);
    rect(3*border+80,2*border+80,80,80);
    fill(#08016C);   //lighter
    //rect(3*border+2*80,2*border+80,80,80);
    fill(#0C5506);    ellipse(3*border+2*80+40,2*border+80+40,85,85);
        fill(#FC0D0D); text(c4,3*border+10+2*80,2*border+80+35);
                fill(#24201D); text(s4,3*border+10+2*80,2*border+80+55);
    fill(#9792E5);  // light   fill(#ED1F37)   //dark   fill(#F0020A);  //darkest.
    //rect(3*border+3*80,2*border+80,80,80);
        fill(#BBE5B7);ellipse(3*border+3*80+40,2*border+80+40,65,65);
        fill(#FC0D0D); text(c5,3*border+10+3*80,2*border+80+35);
                fill(#24201D); text(s5,3*border+10+3*80,2*border+80+55);
    fill(#261F8E);  // light   fill(#ED1F37)   //dark   fill(#F0020A);  //darkest.
    rect(3*border,2*border+2*80,80,80);
        fill(#24201D); text(s2,3*border+10,2*border+35+2*80);
                fill(#FC0D0D); text(c2,3*border+10,2*border+55+2*80);
    fill(#08016C);   //lightet
    rect(3*border+80,2*border+2*80,80,80);
        fill(#24201D); text(s4,3*border+10+80,2*border+35+2*80);
                fill(#FC0D0D); text(c4,3*border+10+80,2*border+55+2*80);
    fill(255);
    //fill(#FA909C);   //lighter
    //rect(3*border+2*80,2*border+2*80,80,80);
    fill(#6D67CB);  // light   fill(#ED1F37)   //dark   fill(#F0020A);  //darkest.
    //rect(3*border+3*80,2*border+2*80,80,80);
        fill(#6FBC68);ellipse(3*border+3*80+40,2*border+2*80+40,70,70);
        fill(#FC0D0D); text(c6,3*border+10+3*80,2*border+35+2*80);
                fill(#24201D); text(s6,3*border+10+3*80,2*border+55+2*80);
     fill(#C1BEF7);  // light   fill(#ED1F37)   //dark   fill(#F0020A);  //darkest.
    rect(3*border,2*border+3*80,80,80);
    fill(#24201D); text(s3,3*border+10,2*border+35+3*80);
        fill(#FC0D0D); text(c3,3*border+10,2*border+55+3*80);
    fill(#9792E5);   //lightest
    rect(3*border+80,2*border+3*80,80,80);
    fill(#24201D); text(s5,3*border+10+80,2*border+35+3*80);
        fill(#FC0D0D); text(c5,3*border+10+80,2*border+55+3*80);
    fill(#6D67CB);   //lighter
    rect(3*border+2*80,2*border+3*80,80,80);
    fill(#24201D); text(s6,3*border+10+2*80,2*border+35+3*80);
        fill(#FC0D0D); text(c6,3*border+10+2*80,2*border+55+3*80);
  //  fill(#F5596B);  // light   fill(#ED1F37)   //dark   fill(#F0020A);  //darkest.
    fill(255);
    rect(3*border+3*80,2*border+3*80,80,80);
    noFill();
}
void labels(){
    fill(0);
        text("CORRGRAM",35,30);
    text(table.getColumnTitle(0),border,120-border);
    text(table.getColumnTitle(1),border,200-border);
    text(table.getColumnTitle(2),border,280-border);
    text(table.getColumnTitle(3),border,360-border);
    
    text(table.getColumnTitle(0),80,height/2-border);
    text(table.getColumnTitle(1),160,height/2-border);
    text(table.getColumnTitle(2),240,height/2-border);
    text(table.getColumnTitle(3),320,height/2-border);
    noFill();
}

float corr(int idx0,int idx1){
  float xsum=0,ysum=0,xmean=0,ymean=0,result=0,xstd=0,ystd=0,covXY=0;
  for (int i = 0; i < table.getRowCount(); i++) {
        TableRow r = table.getRow(i);
        xsum = xsum + r.getFloat(idx0);
      ysum = ysum + r.getFloat(idx1);
  }
      xmean = xsum/table.getRowCount(); println("xmean="+ xmean);
      ymean = ysum/table.getRowCount(); println("ymean="+ ymean);
    for (int i = 0; i < table.getRowCount(); i++) {
        TableRow r = table.getRow(i);
        float x1 =r.getFloat(idx0);
        float y1 =r.getFloat(idx1);
      xstd = (xstd+((x1-xmean)*(x1-xmean)));
      ystd = (ystd+((y1-ymean)*(y1-ymean))); 
      covXY = (covXY + ((x1-xmean)*(y1-ymean)));   
    }
    xstd = sqrt((xstd/table.getRowCount()));   println("xstd="+ xstd);
    ystd = sqrt((ystd/table.getRowCount()));   println("ystd=" + ystd);
    covXY = (covXY/table.getRowCount());       println("covXY="+ covXY);
          result = covXY/(xstd*ystd);
        println(result);
        println(idx0);println(idx1);
    return result;
}

float corr1(int idx0,int idx1){
  float xsum=0,ysum=0,xmean=0,ymean=0,result=0,xstd=0,ystd=0,covXY=0,x,y,p1,q1;
  for (int i = 0; i < table.getRowCount(); i++) {
      x=0;y=0;p1=0;q1=0;
              TableRow r = table.getRow(i); 
      for (int j = 0; j < table.getRowCount(); j++) {
                TableRow s = table.getRow(j);
        if(r.getFloat(idx0) < s.getFloat(idx0)){
          x=x+1; 
         }
        if(r.getFloat(idx0)==s.getFloat(idx0)) {
           y=y+1; 
         }
         if(r.getFloat(idx1)<s.getFloat(idx1)){
          p1++;
         }
         if(r.getFloat(idx1)==s.getFloat(idx1)) {
           q1++;
         }
       }
  rank[i]=x+(y-1)*0.5; 
  rank1[i]=p1+(q1-1)*0.5;
        }
  for (int i = 0; i < table.getRowCount(); i++) {
        TableRow r = table.getRow(i);
        xsum = xsum + rank[i];
      ysum = ysum + rank1[i];
  }
      xmean = xsum/table.getRowCount(); println("xmean="+ xmean);
      ymean = ysum/table.getRowCount(); println("ymean="+ ymean);
    for (int i = 0; i < table.getRowCount(); i++) {
        TableRow r = table.getRow(i);
        float x1 =rank[i];
        float y1 =rank1[i];
      xstd = (xstd+((x1-xmean)*(x1-xmean)));
      ystd = (ystd+((y1-ymean)*(y1-ymean))); 
      covXY = (covXY + ((x1-xmean)*(y1-ymean)));   
    }
    xstd = sqrt((xstd/table.getRowCount()));   println("xstd="+ xstd);
    ystd = sqrt((ystd/table.getRowCount()));   println("ystd=" + ystd);
    covXY = (covXY/table.getRowCount());       println("covXY="+ covXY);
          result = covXY/(xstd*ystd);
        println(result);
        println(idx0);println(idx1);
    return result;
}
}