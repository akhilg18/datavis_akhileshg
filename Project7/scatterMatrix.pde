class scatterMatrix extends Frame {
  
    Table data;
    int idx0=0, idx1=1;
    float minX1,minX2,minX3,minX4;
    float maxX1,maxX2,maxX3,maxX4;
    scatterMatrix( Table data ){
     this.data = data;
     minX1 = min(data.getFloatColumn(0));
     minX2 = min(data.getFloatColumn(1));
     minX3 = min(data.getFloatColumn(2));
     minX4 = min(data.getFloatColumn(3));

     
     maxX1 = max(data.getFloatColumn(0));
     maxX1 = max(data.getFloatColumn(1));
     maxX1 = max(data.getFloatColumn(2));
     maxX1 = max(data.getFloatColumn(3));

 
  }
  
void  draw() {
  stroke(0);
  fill(0);
  line(180,height/2+20,600-20,height/2+20);
  line(180,height-80,600-20,height-80);
  line(180,420,180,720);
  line(280,420,280,720);
  line(380,420,380,720);
  line(480,420,480,720);
  text(table.getColumnTitle(0),200,420);
    text(table.getColumnTitle(1),300,420);
    text(table.getColumnTitle(2),400,420);
    text(table.getColumnTitle(3),500,420);
  
  line(580,420,580,720);    
  line(180,495,580,495);
  line(180,570,580,570);
  line(180,645,580,645);
  
  text(table.getColumnTitle(0),130,460);
    text(table.getColumnTitle(1),130,530);
    text(table.getColumnTitle(2),130,610);
    text(table.getColumnTitle(3),130,685);
  for (int i = 0; i < table.getRowCount()/4; i++) {
        TableRow r = table.getRow(i);
       float X1 = map(r.getFloat(0), minX1, maxX1,180+100,280+70);   
       float X2 = map(r.getFloat(1), minX2, maxX2,280+100,380+70);   
       float X3 = map(r.getFloat(2), minX3, maxX3,380+100,480+70);   
       float X4 = map(r.getFloat(3), minX4, maxX4,480+100,580+70);  
       
       float Y1 = map(r.getFloat(0), minX1, maxX1,420+100-40,495+70-40);
       float Y2 = map(r.getFloat(1), minX2, maxX2,495+60,570+30);
       float Y3 = map(r.getFloat(2), minX3, maxX3,570+60,645+30);
       float Y4 = map(r.getFloat(3), minX4, maxX4,645+60,720+30);
       
       fill(#F7F714);
      ellipse(X2,Y1,2,2);    //plot between SATV,SATM
      fill(#F314F7);
      ellipse(X3,Y1,2,2);    //plot betweem ACT,SATM
      fill(#14D7F7);
      ellipse(X4,Y1,2,2);    //plot betweem GPA,SATM
      
     fill(#14F737);
      ellipse(X1,Y2,2,2);    //plot betweem SATM,SATV
      fill(#F7143E);
      ellipse(X3,Y2,2,2);    //plot betweem ACT,SATV
      fill(#F77714);
      ellipse(X4,Y2,2,2);    //plot betweem GPA,SATV
      fill(#8C14F7);
      ellipse(X1,Y3,2,2);    //plot betweem SATM,ACT
      fill(#D8F714);
      ellipse(X2,Y3,2,2);    //plot betweem SATV,ACT
      fill(#C3CCD8);
      ellipse(X4,Y3,2,2);    //plot betweem GPA,ACT
      fill(#FC8A97);
      ellipse(X1,Y4,2,2);    //plot betweem SATM,GPA
      fill(#2D550E);
      ellipse(X2,Y4,2,2);    //plot betweem SATV,GPA
      fill(#0F0101);
      ellipse(X3,Y4,2,2);    //plot betweem ACT,GPA
      noFill();
  }
      fill(0);
        text("SCATTERPLOT",30,520);
        text("MATRIX",50,535);
        noFill();
}
}