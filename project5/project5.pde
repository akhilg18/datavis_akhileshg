/*Akhilesh Gundaboina(U25998227)*/

Table table;
String filename;
String[] rawdata;
int margin;
float[] column1,column2,column3,column4;

void setup()
{
size(800,400); 
background(#D9D5E0);
stroke(3);
selectInput("Select a file to process:", "fileSelected");
while(filename==null){
delay(1000);}
fill(0);
textSize(30);
text("Enter 0 for parallel coordinates plot",150,100);
readData();  // function to read data from CSV file.
margin = 40;
}


void readData(){
  rawdata  = loadStrings(filename); 
  column1 = new float[rawdata.length-1];
  column2 = new float[rawdata.length-1];
  column3 = new float[rawdata.length-1];
  column4 = new float[rawdata.length-1];
  for(int i=1; i<rawdata.length; i++){
    String[] thisRow = split(rawdata[i], ",");
    column1[i-1] = float(thisRow[0]);
    column2[i-1] = float(thisRow[1]);
    column3[i-1] = float(thisRow[2]);
    column4[i-1] = float(thisRow[3]);
  }
}


void fileSelected(File selection)
{ if (selection == null) 
     { println("Window was closed or the user hit cancel.");
    selectInput("Select a file to process:", "fileSelected");
  } else {
    println("User selected " + selection.getAbsolutePath());
    table = loadTable( selection.getAbsolutePath(), "header" );
    filename = selection.getName();
    ArrayList<Integer> useColumns = new ArrayList<Integer>();
    for(int i = 0; i < table.getColumnCount(); i++){
      if( !Float.isNaN( table.getRow( 0 ).getFloat(i) ) ){
        println( i + " - type float" );
        useColumns.add(i);
      }
      else{
        println( i + " - type string" );
      }
    }
  }
}

void draw(){
  textSize(10);
  keyPressed();
  mouseMoved();
}

void keyPressed(){
 println(key); 
 if (key == '0'){
   background(255);
   stroke(0);
   textSize(25);
   text("PARALLEL COORDINATES",width/3,margin-10);
   textSize(10);
   text("Enter 1 to swap column1 and column2",width/11,height-margin-10);
   text("Enter 2 to swap column2 and column3",width/2-2*margin,height-margin-10);
   text("Enter 3 to swap column3 and column4",width-6*margin,height-margin-10);
   rect(margin,margin,width-2*margin,height-2*margin);
  line(3*margin,70,3*margin,height-2*margin);
  fill(0);
  text(table.getColumnTitle(0),3*margin-15,margin+20);
  noFill();
  for(int i=0;i<rawdata.length-1;i++) 
  {
    stroke(24,34,56);
    float y = map(column1[i],min(column1),max(column1),height-2*margin,3*margin);
    ellipse(3*margin,y,3,3);
  
  line(8*margin,70,8*margin,height-2*margin);
  fill(0);
  text(table.getColumnTitle(1),8*margin-15,margin+20);
  noFill();
    stroke(56,68,87);
    float y2 = map(column2[i],min(column2),max(column2),height-2*margin,3*margin);
    ellipse(8*margin,y2,3,3);
 
  
  line(13*margin,70,13*margin,height-2*margin);
  fill(0);
  text(table.getColumnTitle(2),13*margin-15,margin+20);
  noFill();
    stroke(56,68,87);
    float y3 = map(column3[i],min(column3),max(column3),height-2*margin,3*margin);
    ellipse(13*margin,y3,3,3);
 
  line(17*margin,70,17*margin,height-2*margin);
  fill(0);
  text(table.getColumnTitle(3),17*margin-15,margin+20);
  noFill();
    stroke(56,68,87);
    float y1 = map(column4[i],min(column4),max(column4),height-2*margin,3*margin);
    ellipse(17*margin,y1,3,3);
    
    stroke(34,83,123);
    line(3*margin,y,8*margin,y1);
    stroke(234,65,98);
    line(8*margin,y1,13*margin,y2);
    stroke(67,143,76);
    line(13*margin,y2,17*margin,y3);
    noStroke();
}
   
 }
 
 if (key == '1'){
   background(255);
   stroke(0);
   textSize(25);
   text("PARALLEL COORDINATES",width/3,margin-10);
   textSize(10);
   text("Enter 0 to Reset                   ",width/11,height-margin-10);
   text("Enter 2 to swap column2 and column3",width/2-2*margin,height-margin-10);
   text("Enter 3 to swap column3 and column4",width-6*margin,height-margin-10);
   rect(margin,margin,width-2*margin,height-2*margin);
  line(3*margin,70,3*margin,height-2*margin);
  fill(0);
  text(table.getColumnTitle(1),3*margin-15,margin+20);
  noFill();
  for(int i=0;i<rawdata.length-1;i++) 
  {
    stroke(24,34,56);
    float y = map(column2[i],min(column2),max(column2),height-2*margin,3*margin);
    ellipse(3*margin,y,3,3);
  
  line(8*margin,70,8*margin,height-2*margin);
  fill(0);
  text(table.getColumnTitle(0),8*margin-15,margin+20);
  noFill();
    stroke(56,68,87);
    float y2 = map(column1[i],min(column1),max(column1),height-2*margin,3*margin);
    ellipse(8*margin,y2,3,3);
 
  
  line(13*margin,70,13*margin,height-2*margin);
  fill(0);
  text(table.getColumnTitle(2),13*margin-15,margin+20);
  noFill();
    stroke(56,68,87);
    float y3 = map(column3[i],min(column3),max(column3),height-2*margin,3*margin);
    ellipse(13*margin,y3,3,3);
    
  line(17*margin,70,17*margin,height-2*margin);
  fill(0);
  text(table.getColumnTitle(3),17*margin-15,margin+20);
  noFill();
    stroke(56,68,87);
    float y1 = map(column4[i],min(column4),max(column4),height-2*margin,3*margin);
    ellipse(17*margin,y1,3,3);
    
    stroke(123,86,94);
    line(3*margin,y,8*margin,y1);
    stroke(76,243,76);
    line(8*margin,y1,13*margin,y2);
    stroke(47,84,214);
    line(13*margin,y2,17*margin,y3);
    noStroke();
}
 }
 
 if (key == '2'){
   background(255);
   stroke(0);
   textSize(25);
   text("PARALLEL COORDINATES",width/3,margin-10);
   textSize(10);
   text("Enter 1 to swap column1 and column2",width/11,height-margin-10);
   text("Enter 0 to Reset      ",width/2-2*margin,height-margin-10);
   text("Enter 3 to swap column3 and column4",width-6*margin,height-margin-10);
   rect(margin,margin,width-2*margin,height-2*margin);
  line(3*margin,70,3*margin,height-2*margin);
  fill(0);
  text(table.getColumnTitle(0),3*margin-15,margin+20);
  noFill();
  for(int i=0;i<rawdata.length-1;i++) 
  {
    stroke(24,34,56);
    float y = map(column1[i],min(column1),max(column1),height-2*margin,3*margin);
    ellipse(3*margin,y,3,3);
  
  line(8*margin,70,8*margin,height-2*margin);
  fill(0);
  text(table.getColumnTitle(2),8*margin-15,margin+20);
  noFill();
    stroke(56,68,87);
    float y2 = map(column3[i],min(column3),max(column3),height-2*margin,3*margin);
    ellipse(8*margin,y2,3,3);
 
  
  line(13*margin,70,13*margin,height-2*margin);
  fill(0);
  text(table.getColumnTitle(1),13*margin-15,margin+20);
  noFill();
    stroke(56,68,87);
    float y3 = map(column2[i],min(column2),max(column2),height-2*margin,3*margin);
    ellipse(13*margin,y3,3,3);
    
  line(17*margin,70,17*margin,height-2*margin);
  fill(0);
  text(table.getColumnTitle(3),17*margin-15,margin+20);
  noFill();
    stroke(56,68,87);
    float y1 = map(column4[i],min(column4),max(column4),height-2*margin,3*margin);
    ellipse(17*margin,y1,3,3);
    
    stroke(45,79,243);
    line(3*margin,y,8*margin,y1);
    stroke(198,67,123);
    line(8*margin,y1,13*margin,y2);
    stroke(87,213,9);
    line(13*margin,y2,17*margin,y3);
    noStroke();
}
 }
 
 if (key == '3'){
   background(255);
   stroke(0);
   textSize(25);
   text("PARALLEL COORDINATES",width/3,margin-10);
   textSize(10);
   text("Enter 1 to swap column1 and column2",width/11,height-margin-10);
   text("Enter 2 to swap column2 and column3",width/2-2*margin,height-margin-10);
   text("Enter 0 to Reset      ",width-6*margin,height-margin-10);
   rect(margin,margin,width-2*margin,height-2*margin);
  line(3*margin,70,3*margin,height-2*margin);
  fill(0);
  text(table.getColumnTitle(0),3*margin-15,margin+20);
  noFill();
  for(int i=0;i<rawdata.length-1;i++) 
  {
    stroke(24,34,56);
    float y = map(column1[i],min(column1),max(column1),height-2*margin,3*margin);
    ellipse(3*margin,y,3,3);
  
  line(8*margin,70,8*margin,height-2*margin);
  fill(0);
  text(table.getColumnTitle(1),8*margin-15,margin+20);
  noFill();
    stroke(56,68,87);
    float y2 = map(column2[i],min(column2),max(column2),height-2*margin,3*margin);
    ellipse(8*margin,y2,3,3);
 
  
  line(13*margin,70,13*margin,height-2*margin);
  fill(0);
  text(table.getColumnTitle(3),13*margin-15,margin+20);
  noFill();
    stroke(56,68,87);
    float y3 = map(column4[i],min(column4),max(column4),height-2*margin,3*margin);
    ellipse(13*margin,y3,3,3);
    
  line(17*margin,70,17*margin,height-2*margin);
  fill(0);
  text(table.getColumnTitle(2),17*margin-15,margin+20);
  noFill();
    stroke(56,68,87);
    float y1 = map(column3[i],min(column3),max(column3),height-2*margin,3*margin);
    ellipse(17*margin,y1,3,3);
    
    stroke(4,56,175);
    line(3*margin,y,8*margin,y1);
    stroke(129,54,67);
    line(8*margin,y1,13*margin,y2);
    stroke(34,234,3);
    line(13*margin,y2,17*margin,y3);
    noStroke();
}
 }
}

void mouseMoved(){
 println(mouseX,mouseY);
 text(mouseX+","+mouseY,width/2,height/4-12);
}